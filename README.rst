========================
Accenta RL Agent Example
========================

Copyright (c) 2019-2023 Accenta (www.accenta.ai)

* Web site: https://gitlab.com/ens-data-challenge/accenta-rl-agent-example
* Online documentation: https://ens-data-challenge.gitlab.io/accenta-rl-agent-example
* Examples: https://ens-data-challenge.gitlab.io/accenta-rl-agent-example/gallery/
* Issue tracker: https://gitlab.com/ens-data-challenge/accenta-rl-agent-example/issues


Description
===========

Accenta RL Agent Example for ENS Challenge Data.

The environment on which to train your agent must be installed via the "pip" command like any other Python library: `pip install rlenv`.
You have to write and train your agent on your machine and once you are satisfied with the result, you can publish it for evaluation on the ENS platform via a Git repository that you will have previously created on GitHub or GitLab.

The choice of implementation of the agent is very free both technically and algorithmically.

The only constraint is to implement `a predefined function <https://ens-data-challenge.gitlab.io/accenta-rl-agent-example/api_agent.html#rlagent.agent.load_agent>`_ in Python in the code of your model so that the evaluation platform can load your trained agent.


Dependencies
============

C.f. requirements.txt

.. _install:

Installation
============

Posix (Linux, MacOSX, WSL, ...)
-------------------------------

From the RL Agent source code::

    conda deactivate         # Only if you use Anaconda...
    python3 -m venv env
    source env/bin/activate
    python3 -m pip install --upgrade pip
    python3 -m pip install -r requirements.txt
    python3 setup.py develop


Windows
-------

From the RL Agent source code::

    conda deactivate         # Only if you use Anaconda...
    py -m venv env
    .\env\Scripts\activate
    py -m pip install --upgrade pip
    py -m pip install -r requirements.txt
    py setup.py develop


Example usage
=============

The following commands assume you are in Posix environment (Linux, MacOSX, WSL, ...).
For Windows users, replace `python3` with `py`.
These commands assume that the previously installed venv has been activated.
If not, type `source env/bin/activate` (Posix) or `.\env\Scripts\activate` (Windows). 

Define, train and save the agent::

    python3 examples/print_train_and_save_agent.py


Define, train and save the agent with TensorBoard::

    python3 examples/print_train_and_save_agent_with_tensorboard.py
    tensorboard --logdir ./runs


Assess the "default" agent (the one loaded with the `rlagent.load_agent()` function)::

    python3 examples/print_load_and_eval_agent.py
    

C.f. https://ens-data-challenge.gitlab.io/accenta-rl-agent-example/gallery/ for more examples.


Build and run the Accenta RL Agent Example from Docker (experimental)
=====================================================================

As an alternative to the above procedures, you can use Docker (www.docker.com) to build and test your agent.

Build the docker image
----------------------

From the RL Agent source code::

    docker build -t rlagent .

Assess the "default" agent (the one loaded with the rlagent.load_agent() function) from the docker container
------------------------------------------------------------------------------------------------------------

From the RL Agent source code::

    docker run rlagent python3 /rlagent/examples/print_load_and_eval_agent.py

Train and save the agent from the docker container (fast training)
------------------------------------------------------------------

From the RL Agent source code::

    docker run rlagent python3 /rlagent/examples/print_train_and_save_agent.py

Train and save the agent from the docker container (longer training with tensorboard)
-------------------------------------------------------------------------------------

From the RL Agent source code::

    docker volume create tensorboard
    docker run -v tensorboard:/rlagent/runs rlagent python3 /rlagent/examples/print_train_and_save_agent_with_tensorboard.py
    docker run -v tensorboard:/rlagent/runs --network=host rlagent tensorboard --logdir ./runs/ --bind_all



Bug reports
===========

To search for bugs or report them, please use the Accenta's RL Agent Example Bug Tracker at:

    https://gitlab.com/ens-data-challenge/accenta-rl-agent-example/issues
